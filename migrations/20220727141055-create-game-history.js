'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('GameHistories', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      RoomId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Rooms',
          key: 'id',
        },
        onUpdate: 'cascade',
        onDelete: 'cascade',
      },
      player1Choosen: {
        type: Sequelize.STRING,
      },
      player2Choosen: {
        type: Sequelize.STRING,
      },
      winner: {
        type: Sequelize.INTEGER,
        references: {
          model: 'user_game',
          key: 'id',
        },
        onUpdate: 'cascade',
        onDelete: 'cascade',
      },

      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('GameHistories');
  },
};
