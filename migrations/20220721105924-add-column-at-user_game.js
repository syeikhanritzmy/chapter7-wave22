'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addColumn('user_games', 'role', { type: Sequelize.STRING });
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    queryInterface.removeColumn('user_games', 'role', {});
  },
};
