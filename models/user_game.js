'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      user_game.belongsTo(models.Room);
      user_game.hasMany(models.UserRoom);
      user_game.belongsTo(models.user_game_biodata, {
        foreignKey: 'userGameBiodatumId',
        as: 'userBiodata',
      });

      user_game.hasMany(models.GameHistory, { foreignKey: 'winner' });
    }
  }
  user_game.init(
    {
      username: DataTypes.STRING,
      password: DataTypes.STRING,
      userGameBiodatumId: DataTypes.INTEGER,
      role: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'user_game',
    }
  );
  return user_game;
};
