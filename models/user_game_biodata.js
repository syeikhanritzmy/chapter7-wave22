'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      user_game_biodata.hasOne(models.user_game, {
        onDelete: 'CASCADE',
        
      });
    }
  }
  user_game_biodata.init(
    {
      nama: DataTypes.STRING,
      nohp: DataTypes.STRING,
      email: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'user_game_biodata',
    }
  );
  return user_game_biodata;
};
