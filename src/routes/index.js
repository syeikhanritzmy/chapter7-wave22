const express = require('express');
const router = express.Router();
const UserGame = require('./user_game.routes');
router.use('', UserGame);

module.exports = router;
