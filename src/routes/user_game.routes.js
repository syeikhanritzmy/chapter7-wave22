const express = require('express');
const router = express.Router();
const { UserGameController, RoomController } = require('../../controllers');
const Middleware = require('../../middleware');

router.get('/usergame', Middleware.author, UserGameController.getAllUser);
router.get('/usergame/:id', UserGameController.getUserById);
router.post('/register', UserGameController.createUser);
router.put('/edit/:id', UserGameController.updateUser);
router.delete('/:id', UserGameController.deleteUser);
router.post('/login', UserGameController.login);

router.get('/login-page', UserGameController.loginPage);

router.get('/home', Middleware.author, UserGameController.homePage);

// Room Controller
router.post('/room', RoomController.createRoom);
router.post('/fight/:id', Middleware.authen, RoomController.registerRoom);
module.exports = router;
