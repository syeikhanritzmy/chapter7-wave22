const { Room, UserRoom, GameHistory } = require('../models');

const createRoom = async (req, res, next) => {
  try {
    const { nama } = req.body;
    await Room.create({ name: nama });
    res.status(200).json({
      message: 'success create room',
    });
  } catch (error) {}
};

const registerRoom = async (req, res, next) => {
  try {
    console.log(req.headers.userLogin);
    const { id } = req.headers.userLogin;
    const roomId = req.params.id;
    const input = req.body.input;
    await UserRoom.create({ userGameId: id, RoomId: roomId });
    const room = await GameHistory.findOne({
      where: {
        RoomId: roomId,
      },
    });

    const roomHistory = room;

    if (roomHistory) {
      const p1 = room.dataValues.player1Choosen;
      console.log(p1);
    } else {
      await GameHistory.create({
        RoomId: roomId,
        player1Choosen: input,
      });
    }

    res.status(200).json({
      message: `welcome to room ${roomId}`,
    });
  } catch (error) {
    console.log(error);
  }
};
module.exports = { createRoom, registerRoom };
