const UserGameController = require('./user_game.controller');
const RoomController = require('./room.controller');
const UserRoomController = require('./userRoom.controller');

module.exports = {
  UserGameController,
  RoomController,
  UserRoomController,
};
