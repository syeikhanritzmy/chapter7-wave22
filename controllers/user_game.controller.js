const express = require('express');
const { Token } = require('../utils');
const {
  user_game,
  user_game_biodata,
  user_game_history,
} = require('../models');

const getAllUser = async (req, res) => {
  const userLogin = req.headers.userLogin;
  console.log(userLogin);
  const User = await user_game.findAll({
    include: {
      model: user_game_biodata,
      as: 'userBiodata',
    },
  });
  res.status(200).send({
    status: 200,
    message: 'success get all user',
    data: User,
  });
};

const getUserById = async (req, res) => {
  const User = await user_game.findAll({
    where: {
      id: req.params.id,
    },
    include: {
      model: user_game_biodata,
      as: 'userBiodata',
    },
  });

  res.status(200).send({
    status: 200,
    message: 'get user by id',
    data: User,
  });
};

const createUser = async (req, res, next) => {
  try {
    const { nama, email, nohp, username, password, role } = req.body;

    if (password.length < 8) {
      res.status(400).json({
        message: 'please check your password again',
      });
    } else {
      const userGameBiodata = await user_game_biodata.create({
        nama,
        email,
        nohp,
      });
      const user = await user_game.create({
        username,
        password,
        userGameBiodatumId: userGameBiodata.id,
        role,
      });

      res.status(200).send({
        status: 200,
        message: 'success registers data',
      });
    }
  } catch (error) {
    console.log(error);
    res.status(200).json({
      message: 'Something Wrong',
    });
  }
};

const updateUser = async (req, res) => {
  const updateUser = await user_game.update(req.body, {
    where: {
      id: req.params.id,
    },
  });
  const updateUserGameBiodata = await user_game_biodata.update(req.body, {
    where: {
      id: req.params.id,
    },
  });
  res.status(200).send({
    status: 200,
    message: 'success updating data',
    data: {
      updateUser,
      updateUserGameBiodata,
    },
  });
};
const deleteUser = async (req, res) => {
  await user_game.destroy({
    where: {
      id: req.params.id,
    },
  });
  await user_game_biodata.destroy({
    where: {
      id: req.params.id,
    },
  });
  res.status(200).send({
    status: 200,
    message: 'success delete data',
  });
};
const login = async (req, res) => {
  try {
    const { username, password } = req.body;
    console.log(req.headers, '<<<<< 1');
    const loginUser = await user_game.findOne({
      where: {
        username: username,
      },
    });
    const _id = loginUser.dataValues.id;
    const _password = loginUser.dataValues.password;
    if (password !== _password) {
      res.status(401).json({
        message: 'password or username invalid',
      });
    } else {
      const _role = loginUser.dataValues.role;
      const token = Token.generateToken({ username, role: _role, id: _id });
      if (token) {
        req.headers.token = token;
        console.log(req.headers, '<<<<< 2');
        res.status(200).json({ token });
        res.redirect('home' + '?token=' + token);
      }
    }
  } catch (error) {
    console.log(error);
  }
};
const loginPage = async (req, res, next) => {
  try {
    res.render('login');
  } catch (error) {
    console.log(error);
  }
};

const homePage = async (req, res, next) => {
  try {
    console.log(req.headers, '<<<<< ini adalah login user');
    res.render('home', { user: req.headers.userLogin });
  } catch (error) {
    console.log(error);
  }
};
module.exports = {
  getAllUser,
  getUserById,
  createUser,
  updateUser,
  deleteUser,
  login,
  loginPage,
  homePage,
};
