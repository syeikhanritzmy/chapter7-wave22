const { Token } = require('../utils');

const authentication = (req, res, next) => {
  try {
    const token = req.headers.token || req.body.token;
    if (token) {
      const decode = Token.decodeToken(token);

      if (decode) {
        req.headers.userLogin = decode;
        next();
      } else {
        res.status(401).json({
          message: 'Unathecation',
        });
      }
    } else {
      res.status(401).json({
        message: 'Unathecation',
      });
    }
  } catch (error) {
    console.log(error);
  }
};

const authorization = (req, res, next) => {
  try {
    const token = req.headers.token || req.query.token;
    const decode = Token.decodeToken(token);
    const _role = decode && decode.role ? decode.role : null;
    console.log(decode);
    console.log(_role);
    console.log(token, '<< token');
    if (_role === 'SA') {
      next();
    } else {
      res.status(401).json({
        message: 'Unauth',
      });
    }
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  author: authorization,
  authen: authentication,
};
