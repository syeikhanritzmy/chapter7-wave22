const jwt = require('jsonwebtoken');
const SECRET_KEY = '123456789';
const generateToken = (payload) => {
  try {
    const token = jwt.sign(payload, SECRET_KEY);
    return token;
  } catch (error) {
    console.log(error);
    return null;
  }
};
const decodeToken = (token) => {
  try {
    const payload = jwt.verify(token, SECRET_KEY);
    return payload;
  } catch (error) {
    console.log(error);
    return null;
  }
};
module.exports = { generateToken, decodeToken };
