const express = require('express');
const morgan = require('morgan');
const { sequelize } = require('./models');
const UserGame = require('./models/user_game');
const UserGameBiodata = require('./models/user_game_biodata');

const routes = require('./src/routes');
const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static('controllers'));
// static file

app.use('/img', express.static('public/img'));
app.use('/css', express.static('public/css'));
app.use('/js', express.static('public/js'));

// Templating Engine
app.set('views', './src/views');
app.set('view engine', 'ejs');

app.use(morgan('tiny'));
app.get('/', (req, res) => {
  res.render('index');
});

app.get('/game', (req, res) => {
  res.render('game');
});

app.use('/', routes);
app.use((req, res) => {
  res.render('404.ejs');
});

app.listen(PORT, () => {
  console.log(`server is running PORT : ${PORT}`);
});

// sequelize
//   .sync({ force: true })
//   .then((result) => {
//     console.log(result);
//   })
//   .catch((err) => {
//     console.log(err);
//   });
